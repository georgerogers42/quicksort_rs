use std::io;
use std::fs;
use ::{sort_bible, sort_strings};

#[test]
fn test_sort_bible() {
    let mut v = vec![];
    let f = io::BufReader::new(fs::File::open("TEXT-PCE-127.txt").unwrap());
    sort_bible(false, &mut v, f);
    let s = String::from_utf8(v).unwrap();
    assert!(s.ends_with("Zep 3:20 At that time will I bring you [again], even in the time that I gather you: for I will make you a name and a praise among all people of the earth, when I turn back your captivity before your eyes, saith the LORD.\n"));
    assert!(s.starts_with("1Ch 1:1"));
}
#[test]
fn test_sort_bible_rev() {
    let mut v = vec![];
    let f = io::BufReader::new(fs::File::open("TEXT-PCE-127.txt").unwrap());
    sort_bible(true, &mut v, f);
    let s = String::from_utf8(v).unwrap();
    assert!(s.starts_with("Zep 1:1"));
    assert!(s.ends_with("1Ch 29:30 With all his reign and his might, and the times that went over him, and over Israel, and over all the kingdoms of the countries.\n"));
}

#[test]
fn test_sort_strings() {
    let mut v = vec![];
    let f = io::BufReader::new(fs::File::open("TEXT-PCE-127.txt").unwrap());
    sort_strings(false, &mut v, f);
    let s = String::from_utf8(v).unwrap();
    assert!(s.ends_with("Zep 3:9 For then will I turn to the people a pure language, that they may all call upon the name of the LORD, to serve him with one consent.\n"));
    assert!(s.starts_with("1Ch 10:1"));
}
#[test]
fn test_sort_strings_rev() {
    let mut v = vec![];
    let f = io::BufReader::new(fs::File::open("TEXT-PCE-127.txt").unwrap());
    sort_strings(true, &mut v, f);
    let s = String::from_utf8(v).unwrap();
    assert!(s.starts_with("Zep 3:9"));
    assert!(s.ends_with("1Ch 10:1 Now the Philistines fought against Israel; and the men of Israel fled from before the Philistines, and fell down slain in mount Gilboa.\n"));
}
