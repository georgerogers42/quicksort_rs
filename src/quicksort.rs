fn partition<E, F>(sl: &mut[E], f: F) -> (&mut[E], &mut[E]) where F: Fn(&E, &E) -> bool {
    let l = sl.len();
    let mut p = l / 2;
    let (mut i, mut j) = (0, l);
    loop {
        for x in i..l {
            i = x;
            if f(&sl[p], &sl[i]) {
                break;
            }
        }
        for x in (0..j).rev() {
            j = x;
            if !f(&sl[p], &sl[j]) {
                break;
            }
        }
        if j <= i {
            p = j;
            break;
        }
        sl.swap(i, j);
    }
    let (x, y) = sl.split_at_mut(p);
    let (_, y) = y.split_at_mut(1);
    (x, y)
}

fn do_sort<E>(sl: &mut[E], f: &Fn(&E, &E) -> bool) {
    let l = sl.len();
    if l > 1 {
        let (a, b) = partition(sl, |x, y| f(x, y));
        do_sort(a, f);
        do_sort(b, f);
    }
}

pub fn sort_by<E, F>(sl: &mut[E], f: F) where F: Fn(&E, &E) -> bool {
    do_sort(sl, &f);
}
pub fn sort<E>(sl: &mut[E]) where E: PartialOrd {
    sort_by(sl, |a, b| a <= b);
}
