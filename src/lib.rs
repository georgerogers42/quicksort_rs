#[macro_use]
extern crate lazy_static;
extern crate regex;
extern crate argparse;

use std::io::prelude::*;
use std::io;
use regex::Regex;
use argparse::{ArgumentParser, StoreTrue};
use std::error::Error;
use std::fmt;

pub mod quicksort;

pub use quicksort::{sort, sort_by};

fn get_lines<R: BufRead>(r: R) -> io::Result<Vec<String>> {
    let mut lines = vec![];
    for line in r.lines() {
        let line = line?;
        lines.push(line);
    }
    Ok(lines)
}

#[derive(Debug)]
pub enum ParseError<E> {
    NoMatch,
    Other(E),
}

impl<E> ParseError<E> {
    pub fn new() -> ParseError<E> {
        ParseError::NoMatch
    }
}

impl<E> fmt::Display for ParseError<E> {
    fn fmt(&self, w: &mut fmt::Formatter) -> fmt::Result {
        write!(w, "No Match")
    }
}

impl<E> From<E> for ParseError<E> {
    fn from(e: E) -> Self {
        ParseError::Other(e)
    }
}

impl<E : Error> Error for ParseError<E> {
    fn description(&self) -> &str {
        match self {
            &ParseError::NoMatch => {
                "no match"
            } &ParseError::Other(ref e) => {
                e.description()
            }
        }
    }
    fn cause(&self) -> Option<&Error> {
        match self {
            &ParseError::NoMatch => {
                None
            } &ParseError::Other(ref e) => {
                Some(e)
            }
        }
    }
}

#[derive(Clone, PartialEq, Eq, PartialOrd, Ord, Debug)]
struct Verse {
    book: String,
    chapter: u64,
    verse: u64,
    contents: String,
}

impl fmt::Display for Verse {
    fn fmt(&self, w: &mut fmt::Formatter) -> fmt::Result {
        write!(w, "{} {}:{} {}", self.book, self.chapter, self.verse, self.contents)
    }
}
lazy_static! {
    static ref VERSE: Regex = Regex::new(r"^\s*([a-zA-Z0-9]+)\s+(\d+):(\d+)\s(.+)$").unwrap();
}
fn get_verses<R: BufRead>(r: R) -> Result<Vec<Verse>, ParseError<io::Error>> {
    let mut lines = vec![];
    for line in r.lines() {
        let line = line?;
        let vc = VERSE.captures(&line).ok_or(ParseError::new())?;
        let book = vc[1].to_owned();
        let (chap, verse) = (vc[2].parse().unwrap(), vc[3].parse().unwrap());
        let contents = vc[4].to_owned();
        lines.push(Verse{ book, chapter: chap, verse, contents});
    }
    Ok(lines)
}

fn rev_cverse(a: &Verse, b: &Verse) -> bool {
    (&a.book, b.chapter, b.verse, &a.contents) > (&b.book, a.chapter, a.verse, &b.contents)
}
fn cverse(a: &Verse, b: &Verse) -> bool {
    a < b
}

pub fn sort_app() {
    let (mut reverse, mut bible) = (false, false);
    {
        let mut ap = ArgumentParser::new();
        ap.set_description("mergesort");
        ap.refer(&mut bible).add_option(&["-b", "--bible"], StoreTrue, "Bible Sort");
        ap.refer(&mut reverse).add_option(&["-r", "--reverse"], StoreTrue, "Reverse Sort");
        ap.parse_args_or_exit();
    }
    let stdin = io::stdin();
    let stdout = io::stdout();
    if bible {
        sort_bible(reverse, stdout.lock(), stdin.lock());
    } else {
        sort_strings(reverse, stdout.lock(), stdin.lock());
    }
}

fn sort_bible<W: Write, R: BufRead>(reverse: bool, w: W, r: R) {
    let mut verses = get_verses(r).unwrap();
    if reverse {
        sort_by(&mut verses, rev_cverse);
    } else {
        sort_by(&mut verses, cverse);
    }
    report(w, verses.iter().map(|v| v.to_string())).unwrap();
}

fn sort_strings<W: Write, R: BufRead>(reverse: bool, w: W, r: R) {
    let mut lines = get_lines(r).unwrap();
    sort(&mut lines);
    if reverse {
        report(w, lines.iter().rev()).unwrap();
    } else {
        report(w, lines.iter()).unwrap();
    }
}

fn report<W, Line, Lines>(mut w: W, lines: Lines) -> io::Result<()> where W: Write, Line: AsRef<str>, Lines: Iterator<Item=Line> {
    for line in lines {
        writeln!(w, "{}", line.as_ref())?;
    }
    Ok(())
}

#[cfg(test)]
mod tests;
